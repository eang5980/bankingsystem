<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin Menu</title>
</head>

<style>
input {
	font-size: 20px;
}

body {
	text-align: center;
}
</style>

<body>
	<h2>
		<%
		out.println("Welcome " + session.getAttribute("aName") + " !");
		%>
	</h2>

	<div class="container">

		<input type="button"
			onclick="location.href='/BankApplication/AdminViewCustomerServlet';"
			value="View customers" /> <br> <br> <input type="button"
			onclick="location.href='/BankApplication/AdminViewLoanServlet';"
			value="View loans" /> <br> <br> <input type="button"
			onclick="location.href='/BankApplication/adminpages/admin_updatestatusloan.html';"
			value="Update status of loan" />

	</div>
</body>
</html>