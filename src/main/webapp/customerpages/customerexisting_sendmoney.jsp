<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Send Money</title>
</head>
<style>
input {
	font-size: 20px;
	text-align: center;
}

table {
	margin-left: auto;
	margin-right: auto;
}

body {
	text-align: center;
}
h4{
  line-height: 5px; 
}
</style>

<body>

	<h2>
		<u>Send Money</u>
	</h2>
	<h4>
		Balance:
		<%
	out.println("$"+session.getAttribute("cBalance"));
	%>
	</h4>

	<h4>
		Own Account Number:
		<%
	out.println(session.getAttribute("cAccno"));
	%>
	</h4>

<form action="/BankApplication/CustomerSendMoneyServlet">
	<table>
				<tr>
					<td>To Account Number</td>

				</tr>
				<tr>
					<td><input type="number" name="cToaccno" required></td>
				</tr>
				<tr>
					<td>Amount</td>

				</tr>
				<tr>
					<td><input type="number" name="cTransamt" required></td>
				</tr>



			</table>
			<input type="submit" value="Send">


</form>
</body>
</html>