<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Customer Menu</title>
</head>
<style>
input {
	font-size: 20px;
	text-align: center;
}

body {
	text-align: center;
}
</style>

<body>
	<h2>
		<%
		out.println("Welcome " + session.getAttribute("cName") + " !");
		%>
	</h2>

	<div class="container">

		<input type="button"
			onclick="location.href='/BankApplication/customerpages/customerexisting_changepwd.html';"
			value="Change password" /> <br> <br> <input type="button"
			onclick="location.href='/BankApplication/CustomerCheckBalanceServlet';"
			value="Check balance" /> <br> <br> <input type="button"
			onclick="location.href='/BankApplication/customerpages/customerexisting_sendmoney.jsp';"
			value="Send Money" /> <br> <br> <input type="button"
			onclick="location.href='/BankApplication/customerpages/customerexisting_applyloan.html';"
			value="Apply loan (personal,housing,vehicle)" /> <br> <br>
		<input type="button"
			onclick="location.href='/BankApplication/customerpages/customerexisting_viewtransactionhistory.html';"
			value="View transaction history" />

	</div>
</body>
</html>