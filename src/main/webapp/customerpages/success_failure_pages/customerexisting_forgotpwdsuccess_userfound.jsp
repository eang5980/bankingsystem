<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Forgot Password</title>
</head>
<style>
input {
	font-size: 20px;
	text-align: center;
	
}

table {
	margin-left: auto;
	margin-right: auto;
}

body {
	text-align: center;
}

.cusername,
.csecurityqn{
	background-color: lightgrey;
}
.csecurityqn{
	width:600px;
}
</style>
<body>
	<form action="/BankApplication/CustomerForgotPwdChangePwdServlet">
		<div class="container">
			<h2>Forgot password</h2>
			<table>
				<tr>
					<td>Username</td>
				</tr>
				<tr>
					<td><input type="text" name="cUsername" class="cusername"
						value="<%out.println(session.getAttribute("cUsername"));%>" readonly></td>
				</tr>


				<tr>
					<td>Security Question</td>

				</tr>
				<tr>

					<td><input type="text" name="cSecurityqn" class="csecurityqn"
						value="<%out.println(session.getAttribute("cSecurityqn"));%>" readonly></td>
				</tr>
				
				
				<tr>
					<td>Security Answer</td>

				</tr>
				<tr>

					<td><input type="text" name="cSecurityans" required></td>
				</tr>

				<tr>
					<td>New Password</td>

				</tr>
				<tr>

					<td><input type="text" name="cNewPassword" required></td>
				</tr>
				<tr>
					<td>Confirm Password</td>

				</tr>
				<tr>

					<td><input type="text" name="cConfirmPassword" required></td>
				</tr>
			</table>


		</div>
		<br> <input type="submit" value="Change Password">
	</form>
</body>
</html>