

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class AdminViewCustomerServlet
 */
public class AdminViewCustomerServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		response.setContentType("text/html");

		out.println("<center><html><body>");
		out.println("<h2>Customer Details</h2><br>");
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet res = null;

		try {
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			System.out.println("Driver loaded successfully");

			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/bankdb", "root", "1234");

			System.out.println("Connection established successfully");
			String s = "select * from customer";
			pstmt = con.prepareStatement(s);
			
			res = pstmt.executeQuery();

			out.println("<table border =1>");
			out.println("<tr><th>Name</th><th>Username</th><th>Email</th><th>Phone Number</th><th>Account Number</th><th>Balance</th><tr>");
			while (res.next()) {
				String cName = res.getString(1);
				String cUsername = res.getString(2);
				String cEmail = res.getString(8);
				String cPhonenum = res.getString(9);
				int cAccno = res.getInt(7);
				int cBalance = res.getInt(6);
				
			
				out.println("<tr><td>" + cName + "</td><td>"  +cUsername + "</td><td>"
						+ cEmail + "</td><td>"  +  cPhonenum + "</td><td>"+ cAccno  + "</td><td>" + "$"+cBalance + "</td></tr" );
				
			}
			out.println("</table>");
			out.println("</html></body></center>");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	}


