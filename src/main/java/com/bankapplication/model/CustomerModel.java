package com.bankapplication.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CustomerModel {
	public String getcName() {
		return cName;
	}

	public void setcName(String cName) {
		this.cName = cName;
	}

	public String getcUsername() {
		return cUsername;
	}

	public void setcUsername(String cUsername) {
		this.cUsername = cUsername;
	}

	public String getcPassword() {
		return cPassword;
	}

	public void setcPassword(String cPassword) {
		this.cPassword = cPassword;
	}

	public String getcSecurityans() {
		return cSecurityans;
	}

	public void setcSecurityans(String csecurityans) {
		this.cSecurityans = csecurityans;
	}

	public int getcBalance() {
		return cBalance;
	}

	public void setcBalance(int cBalance) {
		this.cBalance = cBalance;
	}

	public int getcAccno() {
		return cAccno;
	}

	public void setcAccno(int cAccno) {
		this.cAccno = cAccno;
	}

	public String getcSecurityqns() {
		return cSecurityqns;
	}

	public void setcSecurityqns(String cSecurityqns) {
		this.cSecurityqns = cSecurityqns;
	}

	private String cName;
	private String cUsername;
	private String cPassword;
	private String cSecurityans;
	private String cSecurityqns;

	public String getcLoanpersonal() {
		return cLoanpersonal;
	}

	public void setcLoanpersonal(String cLoanpersonal) {
		this.cLoanpersonal = cLoanpersonal;
	}

	public int getcLoanpersonalamt() {
		return cLoanpersonalamt;
	}

	public void setcLoanpersonalamt(int cLoanpersonalamt) {
		this.cLoanpersonalamt = cLoanpersonalamt;
	}

	public String getcLoanhousing() {
		return cLoanhousing;
	}

	public void setcLoanhousing(String cLoanhousing) {
		this.cLoanhousing = cLoanhousing;
	}

	public int getcLoanhousingamt() {
		return cLoanhousingamt;
	}

	public void setcLoanhousingamt(int cLoanhousingamt) {
		this.cLoanhousingamt = cLoanhousingamt;
	}

	public String getcLoanvehicle() {
		return cLoanvehicle;
	}

	public void setcLoanvehicle(String cLoanvehicle) {
		this.cLoanvehicle = cLoanvehicle;
	}

	public int getcLoanvehicleamt() {
		return cLoanvehicleamt;
	}

	public void setcLoanvehicleamt(int cLoanvehicleamt) {
		this.cLoanvehicleamt = cLoanvehicleamt;
	}

	private String cLoanpersonal;
	private int cLoanpersonalamt;
	private String cLoanhousing;
	private int cLoanhousingamt;
	private String cLoanvehicle;
	private int cLoanvehicleamt;

	public String getcLoanstatus() {
		return cLoanstatus;
	}

	public void setcLoanstatus(String cLoanstatus) {
		this.cLoanstatus = cLoanstatus;
	}

	private String cLoanstatus;
	
	public String getcEmail() {
		return cEmail;
	}

	public void setcEmail(String cEmail) {
		this.cEmail = cEmail;
	}

	public String getcPhonenum() {
		return cPhonenum;
	}

	public void setcPhonenum(String cPhonenum) {
		this.cPhonenum = cPhonenum;
	}

	private String cEmail;
	private String cPhonenum;

	private int cBalance;

	private int cAccno;
	private int cTransamt;
	Date date = new Date();
	SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private String cTransdate = format.format(date);

	public String getcFromdate() {
		return cFromdate;
	}

	public void setcFromdate(String cFromdate) {
		this.cFromdate = cFromdate;
	}

	public String getcTodate() {
		return cTodate;
	}

	public void setcTodate(String cTodate) {
		this.cTodate = cTodate;
	}

	private String cFromdate = format.format(date);
	private String cTodate = format.format(date);

	private int cToaccno;
	private String cTousername;
	private int cTobalance;

	private String loanInput;
	private int cLoanamt;

	public int getcLoanamt() {
		return cLoanamt;
	}

	public void setcLoanamt(int cLoanamt) {
		this.cLoanamt = cLoanamt;
	}

	public String getLoanInput() {
		return loanInput;
	}

	public void setLoanInput(String loanInput) {
		this.loanInput = loanInput;
	}

	public int getcTobalance() {
		return cTobalance;
	}

	public void setcTobalance(int cTobalance) {
		this.cTobalance = cTobalance;
	}

	public String getcTousername() {
		return cTousername;
	}

	public void setcTousername(String cTousername) {
		this.cTousername = cTousername;
	}

	public int getcToaccno() {
		return cToaccno;
	}

	public void setcToaccno(int cTooaccno) {
		this.cToaccno = cTooaccno;
	}

	public String getcTransdate() {
		return cTransdate;
	}

	public void setcTransdate(String cTransdate) {
		this.cTransdate = cTransdate;
	}

	public int getcTransamt() {
		return cTransamt;
	}

	public void setcTransamt(int cTransamt) {
		this.cTransamt = cTransamt;
	}

	public String getcCurPassword() {
		return cCurPassword;
	}

	public void setcCurPassword(String cCurPassword) {
		this.cCurPassword = cCurPassword;
	}

	public String getcNewPassword() {
		return cNewPassword;
	}

	public void setcNewPassword(String cNewPassword) {
		this.cNewPassword = cNewPassword;
	}

	public String getcConfirmPassword() {
		return cConfirmPassword;
	}

	public void setcConfirmPassword(String cConfirmPassword) {
		this.cConfirmPassword = cConfirmPassword;
	}

	// For password change
	private String cCurPassword;
	private String cNewPassword;
	private String cConfirmPassword;

	Connection con = null;
	PreparedStatement pstmt = null;
	Statement stmt = null;
	ResultSet res = null;

	public CustomerModel() {
		try {
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			System.out.println("Driver loaded successfully");

			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/bankdb", "root", "1234");

			System.out.println("Connection established successfully");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public int CustomerLogin() {

		try {
			String s = "select * from Customer where cUsername=?";
			pstmt = con.prepareStatement(s);
			pstmt.setString(1, cUsername);
			res = pstmt.executeQuery();
			if (res.next()) {
				if (cPassword.equals(res.getString(3))) {
					cName = res.getString(1);
					cBalance = res.getInt(6);
					cAccno = res.getInt(7);
					return 1;
				} else {
					return 0; // customer username and password does not match
				}
			} else {
				return 0; // customer username not in table
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return 0;
	}

	public int CustomerRegister() {

		try {

			String s = "select * from Customer where cusername=?";
			pstmt = con.prepareStatement(s);
			pstmt.setString(1, cUsername);
			res = pstmt.executeQuery();

			if (res.isBeforeFirst()) {
				return 2; // customer username exist
			}

			s = "insert into Customer(Cname,cUsername,cPassword,Csecurityqn,Csecurityans,Cbalance,cEmail,cPhonenum)  values(?,?,?,?,?,?,?,?)";
			pstmt = con.prepareStatement(s);
			pstmt.setString(1, cName);
			pstmt.setString(2, cUsername);
			pstmt.setString(3, cPassword);
			pstmt.setString(4, cSecurityqns);
			pstmt.setString(5, cSecurityans);
			pstmt.setInt(6, 1000);
			pstmt.setString(8, cEmail);
			pstmt.setString(9, cPhonenum);
			pstmt.setString(10, "false");
			pstmt.setInt(11, 0);
			pstmt.setString(12, "false");
			pstmt.setInt(13, 0);
			pstmt.setString(14, "false");
			pstmt.setInt(15, 0);
			pstmt.executeUpdate();

			return 1;

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return 0;

	}

	public int CustomerPasswordChange() {

		try {

			if (cConfirmPassword.equals(cNewPassword) != true) {
				return 2;
			}

			String s = "select * from Customer where cUsername=?";
			pstmt = con.prepareStatement(s);
			pstmt.setString(1, cUsername);
			res = pstmt.executeQuery();
			if (res.next()) {
				if (cCurPassword.equals(res.getString(3))) {
					s = "update Customer set Cpassword=? where Cusername=?";
					pstmt = con.prepareStatement(s);
					pstmt.setString(2, cUsername);
					pstmt.setString(1, cConfirmPassword);
					int result = pstmt.executeUpdate();

					if (result == 1) {
						return 1;
					} else {
						return 0;
					}
				} else {
					return 0; // customer username and password does not match
				}
			} else {
				return 0; // customer username not in table
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return 0;
	}

	public boolean CustomerForgotPasswordFindUser() {

		try {

			String s = "select * from Customer where Cusername=?";
			pstmt = con.prepareStatement(s);
			pstmt.setString(1, cUsername);
			res = pstmt.executeQuery();
			if (res.next()) {
				cSecurityqns = res.getString(4);
				return true;
			} else {
				return false; // customer username not in table
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return false;
	}

	public int CustomerForgotPasswordChange() {

		try {

			if (cConfirmPassword.equals(cNewPassword) != true) {
				return 0;
			}

			String s = "select * from Customer where Cusername=?";
			pstmt = con.prepareStatement(s);
			pstmt.setString(1, cUsername);
			res = pstmt.executeQuery();
			if (res.next()) {
				if (cSecurityans.toLowerCase().equals(res.getString(5).toLowerCase())) {
					s = "update Customer set Cpassword=? where Cusername=?";
					pstmt = con.prepareStatement(s);
					pstmt.setString(2, cUsername);
					pstmt.setString(1, cConfirmPassword);
					int result = pstmt.executeUpdate();

					if (result == 1) {
						return 1;
					} else {
						return 0;
					}
				} else {
					return 0; // customer username and password does not match
				}
			} else {
				return 0; // customer username not in table
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return 0;
	}

	public void checkBalance() {

		try {
			String s = "select * from Customer where cUsername=?";
			pstmt = con.prepareStatement(s);
			pstmt.setString(1, cUsername);
			res = pstmt.executeQuery();

			if (res.next()) {
				cBalance = res.getInt(6);
				cAccno = res.getInt(7);

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public int CustomerSendMoney() {

		try {

			if (cAccno == cToaccno) {

				return 0;
			}

			String s = "select * from Customer where Caccno=?";
			pstmt = con.prepareStatement(s);
			pstmt.setInt(1, cToaccno);
			res = pstmt.executeQuery();
			if (res.next() == false) {
				System.out.println("LOL1");
				return 3;
			}

			s = "select * from Customer where Caccno=?";
			pstmt = con.prepareStatement(s);
			pstmt.setInt(1, cAccno);
			res = pstmt.executeQuery();
			if (res.next()) {
				cBalance = res.getInt(6);
				if (cTransamt > cBalance) {

					return 2; // Transfer amount more than balance left
				} else {

					s = "insert into customer_transaction values(?,?,?,?,?)";
					pstmt = con.prepareStatement(s);
					pstmt.setString(1, cUsername);
					pstmt.setString(2, cTransdate);
					pstmt.setInt(3, cAccno);
					pstmt.setInt(4, cToaccno);
					pstmt.setInt(5, cTransamt);
					pstmt.executeUpdate();

					s = "select * from Customer where Caccno=?";
					pstmt = con.prepareStatement(s);
					pstmt.setInt(1, cToaccno);
					res = pstmt.executeQuery();
					if (res.next()) {
						cTousername = res.getString(2);
						cTobalance = res.getInt(6);
						cTobalance += cTransamt;
					} else {

						return 0;
					}

					s = "update Customer set Cbalance=? where Caccno=?";
					pstmt = con.prepareStatement(s);
					pstmt.setInt(1, cTobalance);
					pstmt.setInt(2, cToaccno);
					pstmt.executeUpdate();

					s = "insert into customer_transaction values(?,?,?,?,?)";
					pstmt = con.prepareStatement(s);
					pstmt.setString(1, cTousername);
					pstmt.setString(2, cTransdate);
					pstmt.setInt(3, cAccno);
					pstmt.setInt(4, cToaccno);
					pstmt.setInt(5, cTransamt);
					pstmt.executeUpdate();

					s = "select * from Customer where Caccno=?";
					pstmt = con.prepareStatement(s);
					pstmt.setInt(1, cAccno);
					res = pstmt.executeQuery();
					if (res.next()) {
						s = "update Customer set Cbalance=? where Caccno=?";
						cBalance -= cTransamt;
						pstmt = con.prepareStatement(s);
						pstmt.setInt(1, cBalance);
						pstmt.setInt(2, cAccno);
						pstmt.executeUpdate();
					}

					return 1;
				}
			} else {

				return 3; // To account number does not exist
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return 0;
	}

	public int ViewTransactionHistory() {

		try {

			if (cFromdate.compareTo(cTodate) > 0) {
				return 2; // From cannot be after To
			}

			String s = "select * from customer_transaction where Ctransdate between ? and ? and Cusername=?";
			pstmt = con.prepareStatement(s);
			pstmt.setString(1, cFromdate);
			pstmt.setString(2, cTodate);
			pstmt.setString(3, cUsername);
			res = pstmt.executeQuery();
			if (res.next()) {
				cAccno = res.getInt(3);
				cToaccno = res.getInt(4);
				cTransamt = res.getInt(5);
				return 1;
			} else {
				return 0; // No transaction history
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return 0;
	}

	public int ApplyLoan() {
		String typeLoanamt = "";
		try {

			
			stmt = con.createStatement();

			String strSQLQuery = String.format("select * from customer where Cusername='%s' and %s='false' or %s='reject'", cUsername,
					loanInput,loanInput);
			res = stmt.executeQuery(strSQLQuery);
			if (res.next()) {

				if (loanInput.equals("cLoanpersonal")) {
					typeLoanamt = "cLoanpersonalamt";
				} else if (loanInput.equals("cLoanhousing")) {
					typeLoanamt = "cLoanhousingamt";
				} else if (loanInput.equals("cLoanvehicle")) {
					typeLoanamt = "cLoanvehicleamt";
				}

				
				strSQLQuery = String.format("update customer set %s='pending', %s=%d where Cusername='%s'", loanInput,
						typeLoanamt, cLoanamt, cUsername);
				stmt.executeUpdate(strSQLQuery);
				return 1;

			} else {
				return 0;
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;

	}
	
	public int UpdateLoanStatus() {
		
		try {

			
			stmt = con.createStatement();

			String strSQLQuery = String.format("select * from customer where Cusername='%s' and (%s='pending' or %s='reject')" , cUsername,loanInput,loanInput);
				
			res = stmt.executeQuery(strSQLQuery);
			if (res.next()) {

				strSQLQuery = String.format("update customer set %s='%s' where Cusername='%s'", loanInput,
						cLoanstatus, cUsername);
				stmt.executeUpdate(strSQLQuery);
				return 1;

			} else {
				return 0;
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;

	}

}
