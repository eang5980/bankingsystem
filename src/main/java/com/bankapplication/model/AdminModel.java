package com.bankapplication.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpSession;

public class AdminModel {

	private String adminName;
	private String adminUsername;
	private String adminPassword;

	public String getAdminName() {
		return adminName;
	}

	public void setAdminName(String adminName) {
		this.adminName = adminName;
	}

	public String getAdminUsername() {
		return adminUsername;
	}

	public void setAdminUsername(String adminUsername) {
		this.adminUsername = adminUsername;
	}

	public String getAdminPassword() {
		return adminPassword;
	}

	public void setAdminPassword(String adminPassword) {
		this.adminPassword = adminPassword;
	}

	Connection con = null;
	PreparedStatement pstmt = null;
	ResultSet res = null;

	public AdminModel() {
		try {
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			System.out.println("Driver loaded successfully");

			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/bankdb", "root", "1234");

			System.out.println("Connection established successfully");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public int AdminLogin() {

		try {
			String s = "select * from Admin where Ausername=?";
			pstmt = con.prepareStatement(s);
			pstmt.setString(1, adminUsername);
			res = pstmt.executeQuery();
			if (res.next()) {
				if (adminPassword.equals(res.getString(3))) {
					adminName = res.getString(1);
					return 1;
				} else {
					return 0; // Admin username and password doesnt match
				}
			} else {
				return 0; // Admin username not in table
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return 0;
	}

}
