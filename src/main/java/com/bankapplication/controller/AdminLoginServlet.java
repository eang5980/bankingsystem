package com.bankapplication.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bankapplication.model.AdminModel;



/**
 * Servlet implementation class AdminLoginServlet
 */
public class AdminLoginServlet extends HttpServlet {
	
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String adminUsername = request.getParameter("adminUsername");
		String adminPassword = request.getParameter("adminPassword");
		

		AdminModel am= new AdminModel();
		
		

		am.setAdminUsername(adminUsername);
		am.setAdminPassword(adminPassword);
		
		int result = am.AdminLogin();
		if(result==1) {
			HttpSession session = request.getSession(true);
			session.setAttribute("aName", am.getAdminName());
			response.sendRedirect("/BankApplication/adminpages/success_failure_pages/admin_loginsuccess_menu.jsp");
		}
		else {
			response.sendRedirect("/BankApplication/adminpages/success_failure_pages/admin_loginfail.html");
		}
	}
}
