package com.bankapplication.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bankapplication.model.CustomerModel;

/**
 * Servlet implementation class CustomerChangePwdServlet
 */
public class CustomerChangePwdServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(true);
		String cUsername = String.valueOf(session.getAttribute("cUsername"));
		
		String cCurPassword = request.getParameter("cCurPassword");
		String cNewPassword = request.getParameter("cNewPassword");
		String cConfirmPassword = request.getParameter("cConfirmPassword");
		
		CustomerModel cm= new CustomerModel();
		
		cm.setcUsername(cUsername);
		cm.setcCurPassword(cCurPassword);
		cm.setcNewPassword(cNewPassword);
		cm.setcConfirmPassword(cConfirmPassword);
		
		int result = cm.CustomerPasswordChange();
		
		if(result==1) {
			response.sendRedirect("/BankApplication/customerpages/success_failure_pages/customerexisting_changepwdsuccess.jsp");
		}
		else if (result ==2) {
			response.sendRedirect("/BankApplication/customerpages/success_failure_pages/customerexisting_changepwdfail_wrongconfirm.html");
		} else {
			response.sendRedirect("/BankApplication/customerpages/success_failure_pages/customerexisting_changepwdfail_wrongcurrent.html");
		}
		
	}

}
