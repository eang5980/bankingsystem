package com.bankapplication.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bankapplication.model.CustomerModel;

/**
 * Servlet implementation class CustomerApplyLoanServlet
 */
public class CustomerApplyLoanServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession(true);
		String cUsername = String.valueOf(session.getAttribute("cUsername"));
		String cLoantype = request.getParameter("cLoantype");
		int cLoanamt = Integer.parseInt(request.getParameter("cLoanamt"));

		CustomerModel cm = new CustomerModel();
		cm.setLoanInput(cLoantype);
		cm.setcLoanamt(cLoanamt);
		cm.setcUsername(cUsername);
		int res = cm.ApplyLoan();
		
		if(res ==1) {
			response.sendRedirect("/BankApplication/customerpages/success_failure_pages/customerexisting_applyloansuccess.jsp");
		}else if (res ==0) {
			response.sendRedirect("/BankApplication/customerpages/success_failure_pages/customerexisting_applyloanfail.html");
		}
	
		
		

	}
}
