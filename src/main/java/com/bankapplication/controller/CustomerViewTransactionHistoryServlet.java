package com.bankapplication.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bankapplication.model.CustomerModel;

/**
 * Servlet implementation class CustomerViewTransactionHistoryServlet
 */
public class CustomerViewTransactionHistoryServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		Date date = new Date();
		date = Calendar.getInstance().getTime();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String cFromdate = format.format(date);
		String cTodate = format.format(date);
		HttpSession session = request.getSession(true);
		String cUsername = (String) session.getAttribute("cUsername");
		cFromdate = request.getParameter("cFromdate");
		cTodate = request.getParameter("cTodate");

		CustomerModel cm = new CustomerModel();
		cm.setcUsername(cUsername);
		cm.setcFromdate(cFromdate);
		cm.setcTodate(cTodate);
		int result = cm.ViewTransactionHistory();

		if (result == 1) {
			session.setAttribute("cUsername", cm.getcUsername());
			session.setAttribute("cFromdate", cm.getcFromdate());
			session.setAttribute("cTodate", cm.getcTodate());
			session.setAttribute("cAccno", cm.getcAccno());
			response.sendRedirect(
					"/BankApplication/Customerexisting_viewtransactionhistorysuccessServlet");
		} else if (result == 2) {
			response.sendRedirect(
					"/BankApplication/customerpages/success_failure_pages/customerexisting_viewtransactionhistoryfail_fromafterto.html");

		} else {
			response.sendRedirect(
					"/BankApplication/customerpages/success_failure_pages/customerexisting_viewtransactionhistoryfail_nohistory.html");
		}
	}

}
