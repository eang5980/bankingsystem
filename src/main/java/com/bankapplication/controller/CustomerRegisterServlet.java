package com.bankapplication.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bankapplication.model.CustomerModel;

/**
 * Servlet implementation class CustomerRegisterServlet
 */
public class CustomerRegisterServlet extends HttpServlet {

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		String cName = request.getParameter("cName");
		String cUsername = request.getParameter("cUsername");
		String cPassword = request.getParameter("cPassword");
		String cSecurityqn = request.getParameter("cSecurityqn");
		String cSecurityans = request.getParameter("cSecurityans");
		String cEmail = request.getParameter("cEmail");
		String cPhonenum= request.getParameter("cPhonenum");
 

		CustomerModel cm = new CustomerModel();

		cm.setcName(cName);
		cm.setcUsername(cUsername);
		cm.setcPassword(cPassword);
		cm.setcSecurityqns(cSecurityqn);
		cm.setcSecurityans(cSecurityans);
		cm.setcEmail(cEmail);
		cm.setcPhonenum(cPhonenum);

		int result = cm.CustomerRegister();

		if (result == 1) {
			response.sendRedirect("/BankApplication/customerpages/success_failure_pages/customernew_registersuccess.jsp");
		} else if (result == 2) {
			response.sendRedirect("/BankApplication/customerpages/success_failure_pages/customernew_registerfail_duplicate.html");
		} else {
			response.sendRedirect("/BankApplication/customerpages/success_failure_pages/customernew_registerfail.html");
		}
	}

}
