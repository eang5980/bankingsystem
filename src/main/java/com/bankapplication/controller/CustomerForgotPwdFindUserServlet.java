package com.bankapplication.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bankapplication.model.CustomerModel;

/**
 * Servlet implementation class CustomerForgetPwdServlet
 */
public class CustomerForgotPwdFindUserServlet extends HttpServlet {
	
       
   @Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	   
		String cUsername = request.getParameter("cUsername");
		
		CustomerModel cm= new CustomerModel();
		
		cm.setcUsername(cUsername);
		
		boolean userFound = cm.CustomerForgotPasswordFindUser();
		
		if(userFound == true) {
			HttpSession session = request.getSession(true);
//			session.setAttribute("cUsername", cm.getcUsername());
//			session.setAttribute("cSecurityqn", cm.getcSecurityqns());
			response.sendRedirect("/BankApplication/customerpages/success_failure_pages/customerexisting_forgotpwdsuccess_userfound.jsp");
		}
		else {
			response.sendRedirect("/BankApplication/customerpages/success_failure_pages/customerexisting_forgotpwdfail_usernotfound.html");
		}
		
	}
}
