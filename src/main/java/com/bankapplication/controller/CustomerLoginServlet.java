package com.bankapplication.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bankapplication.model.AdminModel;
import com.bankapplication.model.CustomerModel;

/**
 * Servlet implementation class CustomerLoginServlet
 */
public class CustomerLoginServlet extends HttpServlet {
	
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String cUsername = request.getParameter("cUsername");
		String cPassword = request.getParameter("cPassword");
		

		CustomerModel cm= new CustomerModel();
		
		

		cm.setcUsername(cUsername);
		cm.setcPassword(cPassword);
		
		int result = cm.CustomerLogin();
		if(result==1) {
			HttpSession session = request.getSession(true);
			session.setAttribute("cName", cm.getcName());
			session.setAttribute("cUsername", cm.getcUsername());
			session.setAttribute("cBalance", cm.getcBalance());
			session.setAttribute("cAccno", cm.getcAccno());
			response.sendRedirect("/BankApplication/customerpages/success_failure_pages/customerexisting_loginsuccess_menu.jsp");
		}
		else {
			response.sendRedirect("/BankApplication/customerpages/success_failure_pages/customerexisting_loginfail.html");
		}
	}

}
