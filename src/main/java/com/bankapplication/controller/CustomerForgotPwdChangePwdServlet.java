package com.bankapplication.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bankapplication.model.CustomerModel;

/**
 * Servlet implementation class CustomerForgotPwdChangePwdServlet
 */
public class CustomerForgotPwdChangePwdServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession(true);
		String cUsername = request.getParameter("cUsername");
		String cSecurityqn = request.getParameter("cSecurityqn");
		String cSecurityans = request.getParameter("cSecurityans");
		String cNewPassword = request.getParameter("cNewPassword");
		String cConfirmPassword = request.getParameter("cConfirmPassword");
		
		CustomerModel cm= new CustomerModel();
		
		cm.setcUsername(cUsername);
		cm.setcNewPassword(cNewPassword);
		cm.setcSecurityqns(cSecurityqn);
		cm.setcSecurityans(cSecurityans);
		cm.setcConfirmPassword(cConfirmPassword);
		
		int result = cm.CustomerForgotPasswordChange();
		
		if(result==1) {
			response.sendRedirect("/BankApplication/customerpages/success_failure_pages/customerexisting_forgotpwdsuccess_userfound_success.jsp");
		}
		 else {
				session.setAttribute("cUsername", cm.getcUsername());
				session.setAttribute("cSecurityqn", cm.getcSecurityqns());
			response.sendRedirect("/BankApplication/customerpages/success_failure_pages/customerexisting_forgotpwdsuccess_userfound_fail.jsp");
		}
	}

}
