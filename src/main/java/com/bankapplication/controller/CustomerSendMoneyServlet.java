package com.bankapplication.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bankapplication.model.CustomerModel;

/**
 * Servlet implementation class CustomerSendMoneyServlet
 */
public class CustomerSendMoneyServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession session = request.getSession(true);
		String cUsername = (String) session.getAttribute("cUsername");
		int cBalance = (int) session.getAttribute("cBalance");
		int cAccno = (int) session.getAttribute("cAccno");
	

		int cToaccno = Integer.parseInt(request.getParameter("cToaccno"));
		int cTransamt = Integer.parseInt(request.getParameter("cTransamt"));
		

		Date date = new Date();
		date = Calendar.getInstance().getTime();
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String cTransdate = format.format(date);
		
		CustomerModel cm= new CustomerModel();
		cm.setcUsername(cUsername);
		cm.setcBalance(cBalance);
		cm.setcAccno(cAccno);
		cm.setcToaccno(cToaccno);
		cm.setcTransamt(cTransamt);
		int result = cm.CustomerSendMoney();
		
		if(result ==1) {
			response.sendRedirect("/BankApplication/customerpages/success_failure_pages/customerexisting_sendmoneysuccess.jsp");
		}else if (result ==2) {
			response.sendRedirect("/BankApplication/customerpages/success_failure_pages/customerexisting_sendmoneyfail_less.jsp");
		}else if (result ==3) {
			response.sendRedirect("/BankApplication/customerpages/success_failure_pages/customerexisting_sendmoneyfail_doesnotexist.jsp");
		}else {
			response.sendRedirect("/BankApplication/customerpages/success_failure_pages/customerexisting_sendmoneyfail_sameacc.jsp");
		}
		
		
	}

}
