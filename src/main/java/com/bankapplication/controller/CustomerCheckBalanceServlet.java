package com.bankapplication.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bankapplication.model.CustomerModel;

/**
 * Servlet implementation class CustomerCheckBalance
 */
public class CustomerCheckBalanceServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession(true);
		String cUsername = (String) session.getAttribute("cUsername");

		CustomerModel cm = new CustomerModel();
		cm.setcUsername(cUsername);
		cm.checkBalance();

		session.setAttribute("cAccno", cm.getcAccno());
		session.setAttribute("cBalance", cm.getcBalance());

		response.sendRedirect("/BankApplication/customerpages/customerexisting_checkbalance.jsp");

	}

}
