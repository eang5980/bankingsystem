package com.bankapplication.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bankapplication.model.CustomerModel;

/**
 * Servlet implementation class AdminUpdateLoanStatusServlet
 */
public class AdminUpdateLoanStatusServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		HttpSession session = request.getSession(true);
		String cUsername = String.valueOf(request.getParameter("cUsername"));
		String cLoantype = request.getParameter("cLoantype");
		String cUpdatestatus = request.getParameter("cUpdatestatus");


		CustomerModel cm = new CustomerModel();
		cm.setLoanInput(cLoantype);
		cm.setcLoanstatus(cUpdatestatus);
		cm.setcUsername(cUsername);
		int res = cm.UpdateLoanStatus();
		
		if(res ==1) {
			response.sendRedirect("/BankApplication/adminpages/success_failure_pages/admin_updatestatusloansuccess.jsp");
		}else if (res ==0) {
			response.sendRedirect("/BankApplication/adminpages/success_failure_pages/admin_updatestatusloanfail.html");
		}
	}

}
