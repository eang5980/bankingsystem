
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bankapplication.model.CustomerModel;

/**
 * Servlet implementation class Customerexisting_viewtransactionhistorysuccess
 */
public class Customerexisting_viewtransactionhistorysuccessServlet extends HttpServlet {

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		response.setContentType("text/html");

		out.println("<center><html><body>");
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet res = null;

		try {
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			System.out.println("Driver loaded successfully");

			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/bankdb", "root", "1234");

			System.out.println("Connection established successfully");
			String s = "select * from customer_transaction where Ctransdate between ? and ? and Cusername=?";
			pstmt = con.prepareStatement(s);
			HttpSession session = request.getSession(true);
			String cUsername = (String) session.getAttribute("cUsername");
			String cFromdate = (String) session.getAttribute("cFromdate");
			String cTodate = (String) session.getAttribute("cTodate");
			int cAccno = (int) session.getAttribute("cAccno"); 
			
			pstmt.setString(1, cFromdate);
			pstmt.setString(2, cTodate+" 23:59:59");
			pstmt.setString(3, cUsername);
			res = pstmt.executeQuery();

			out.println("<table border =1>");
			out.println("<tr><th>Transaction Date</th><th>From Account No</th><th>To Account No</th><th>Amount</th><tr>");
			while (res.next()) {
				String cTransdate = res.getString(2);
				int cFromaccno = res.getInt(3);
				int cToaccno = res.getInt(4);
				int cTransamt = res.getInt(5);
				String sign;
				if(cAccno == cFromaccno) {
					sign= "-";
				} else {
					sign= "+";
				}
				out.println("<tr><td>" + cTransdate + "</td><td>"  +cFromaccno + "</td><td>"
						+ cToaccno + "</td><td>"  +  sign +"$"+cTransamt + "</td></tr" );
				
			}
			out.println("</table>");
			out.println("</html></body></center>");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
