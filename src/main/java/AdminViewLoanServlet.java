

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class AdminViewLoanServlet
 */
public class AdminViewLoanServlet extends HttpServlet {
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		response.setContentType("text/html");

		out.println("<center><html><body>");
		out.println("<h2>View Customers Loan</h2><br>");
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet res = null;

		try {
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			System.out.println("Driver loaded successfully");

			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/bankdb", "root", "1234");

			System.out.println("Connection established successfully");
			String s = "select * from customer";
			pstmt = con.prepareStatement(s);
			
			res = pstmt.executeQuery();

			out.println("<table border =1>");
			out.println("<tr><th>Name</th><th>Username</th><th>Personal Loan</th><th>Personal Loan Amount</th><th>Housing Loan</th><th>Housing Loan Amount</th><th>Vehicle Loan</th><th>Vehicle Loan Amount</th><tr>");
			while (res.next()) {
				String cName = res.getString(1);
				String cUsername = res.getString(2);
				String cLoanpersonal = res.getString(10);
				int cLoanpersonalamt = res.getInt(11);
				String cLoanhousing = res.getString(12);
				int cLoanhousingamt = res.getInt(13);
				String cLoanvehicle = res.getString(14);
				int cLoanvehicleamt = res.getInt(15);
				
			
				out.println("<tr><td>" + cName + "</td><td>"  +cUsername + "</td><td>"
						+ cLoanpersonal + "</td><td>" + "$" +  cLoanpersonalamt + "</td><td>"+ cLoanhousing  + "</td><td>" + "$"+cLoanhousingamt + "</td><td>" +cLoanvehicle + "</td><td>" + "$"+ cLoanvehicleamt  +"</td></tr>" );
				
			}
			out.println("</table>");
			out.println("</html></body></center>");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
